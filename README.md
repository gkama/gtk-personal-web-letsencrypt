## Overview
This project uses the `.gitlab-ci.yml` file to generate a [Let’s Encrypt](https://letsencrypt.org/) certificate to my personal website [https://gkamacharov.com](https://gkamacharov.com)

Full credit goes to Lukas 'Eipi' Eipert: [https://gitlab.com/leipert-projects/gitlab-letsencrypt#usage-with-gitlab-ci](https://gitlab.com/leipert-projects/gitlab-letsencrypt#usage-with-gitlab-ci)